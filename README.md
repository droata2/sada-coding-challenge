# SADA-coding-challenge

I used a data chunk of size 500,000 record out of about 72M because of memory constraints.
The dataset I used was from the Federal Election Comission and it is the Contributions by individuals for [2019-2020](https://www.fec.gov/data/browse-data/?tab=bulk-data)

The output file repeated_donor.txt was formatted according to the [donation-analytics challenge](https://github.com/InsightDataScience/donation-analytics/blob/master/README.md) guidelines.
